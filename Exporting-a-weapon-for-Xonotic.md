_This article looks complete, [old methodology](#old-methodology) isn't a very complete section due to  the obsolescence._

# _Table of Contents_
> ### 1. [Modeling requirements](#modeling-requirements)
> ### 2. [Introduction to export a weapon](#introduction-to-export-a-weapon)
> ### 3. [Textures and UV map](#textures-and-uv-map)
> ### 4. [Starting to export](#starting-to-export)
> > 4.1. [Modifiers](#modifiers)<br/>
> > 4.2. [Bones and animations](#bones-and-animations)<br/>
> > 4.3. [Textures and materials](#textures-and-materials)
> ### 5. [New methodology](#new-methodology)
> > 5.1. [SMD (includes IQM converter and dpmodel guide)](#smd-includes-iqm-converter-and-dpmodel-guide)
> ### 6. [Export troubleshooting](#export-troubleshooting)
> ### 7. [Old methodology](#old-methodology)
> > 7.1. [MD3](#md3)<br/>
> > 7.2. [IQM](#iqm)

# Modeling requirements
### To follow this, you need knowledge about modeling using Blender

Blender version recommended: [**2.79b**](https://download.blender.org/release/Blender2.79/) or [**later**](https://www.blender.org/)

You need [dpmodel](https://icculus.org/twilight/darkplaces/files/) to convert SMD files:
- [dpmodel tool](https://icculus.org/twilight/darkplaces/files/dpmodel20091008beta1.zip)

Requires these following tools (addons) for Blender:
- [IQM exporter](https://gitlab.com/xonotic/iqm) to export IQM files.
- [Source Tools (SMD importer/exporter)](http://steamreview.org/BlenderSourceTools/archives/) to import/export SMD files.

Optional:
- [MD3 importer/exporter](https://github.com/neumond/blender-md3) to import/export MD3 files.

***IMPORTANT NOTE**: if you want to contribute, it's strongly recommended using 2.79b version for compatibility cases, they need to open from this version. Share the .blend file using 2.79b or earlier , make sure to work properly from this version!*<br/>
*If the version of a file is later than 2.79b, maybe some tweaks would need to take and would become tedious.*

# Introduction to export a weapon
**Keep in mind about weapon systems:**

- `g_*.md3`: model mesh for item pickup spawn and third person.
- `h_*.iqm`: for first person animations (if this step used [old methodology](#old-methodology) is a glue of `v_*`); the skeleton and the bones (the rigging and animation part). Keep in mind, you would need `*.iqm.framegroups` (more info: [Framegroups](framegroups), you can look `*.iqm.framegroups` opening a notepad to see what animations contain and the keyframes where these are set)
- `v_*.md3`: model mesh for third person; if `h_*.iqm` doesn't match with the animations needed of `v_*.md3`, it will generate spamming errors and bad pivot position (usually happens in the [old methodology](#old-methodology)).

More info: [Weapon modeling system](Weaponsystem)


# Textures and UV map

### For MD3 exportation:

You have to add single material with single texture for every mesh object. Name of texture node will be written as is into MD3 data (suffixes like .001, .002 are ignored, feel free to use one texture for many meshes).
<br />
Keep in mind, when you've done your UV map in your model, you need to set this:
<br />
<br />
<img src="uploads/efee46a2a80a232ebe5fa63e30f4e2d9/Md3exp.png" alt="Md3exp" width="300" />
<br />
Texture must:

- be of **Image** type
- mapping type **UV**
- have UV map assigned

otherwise resulting data block will be filled with zeroes.
<br />
<br />
<img src="uploads/c48fb7565453eab24faca2d6ffec4347/Md3_textures.png" alt="Md3_textures" width="360" />
<br />
<br />
If you didn't this, the model will be invisible and it won't be able to interact shaders.
<br />
<br />
[Blender reference about UV in MD3](https://archive.blender.org/wiki/index.php/Extensions:2.6/Py/Scripts/Import-Export/MD3/)

# Starting to export

## Modifiers

Keep in mind, you need to set these modifiers for the mesh/model object: 

<img src="uploads/447f6abe0f73c6d6cadce97e4ed66e8b/blendermodifiersiqmandmd3.jpg" alt="blendermodifiersiqmandmd3" />

## Bones and animations

MUST contain `tag_handle`, `tag_shell` and `tag_shot` bone names attached. These tag_names MUST be attached, if not the model will cause errors.

`tag_Somename` is another one, you can add if you need some optional animation in this bone. (Not useful for [old methodology](#old-methodology))

Moreover, watch out about position and rotation. `h_*.iqm` alterates the position and rotation of `v_*.md3`. The positions may not be equal.

`g_*` and `v_*` MUST contain one bone. The animations can't be included. It isn't like `h_*`.

Required animations and in the correct order: 
- `fire`
- `fire2`
- `idle`
- `reload`

## Textures and materials

Please note that before exporting, about textures and materials, you have to assign the same as the script like this example shader file (`crylink.shader` file):
```
crylink_new // your material name
{
	dpreflectcube cubemaps/default/sky
	{
		map textures/crylink_new.tga // the texture you wanna display in-game, make sure to test in Blender!
		rgbgen lightingDiffuse
	}
}
```
Note: `crylink_new` must appear in the material.
<br/><br/>Material:<br/>
<img src="uploads/abef98eaddd7c61bec7eb467efe4c3fe/crylinkblendermaterialsample.jpg" alt="crylinkblendermaterialsample" width=920 />
<br/>

# New methodology

You can make optional customized animations adding bones for the animations.

## SMD (includes IQM converter and dpmodel guide)

Use Source Tools addon in Blender to export the model and animations. 

- For `h_*`:<br/>
Select the mesh and 1 animation and click Export, you need to export all animations one per one: 
<br/><img src="uploads/3775545047aa58d6e4fcb37df4f500a1/blenderSMDexport1.jpg" alt="blenderSMDexport1" width=380 /><br/><br/>
Repeat the process:<br/><img src="uploads/b7c8b250fc1af6b30c999506e7143930/blenderSMDexport2.jpg" alt="blenderSMDexport2" width=350 /><br/><br/>
<img src="uploads/9e9efa19232d1c47fd982ad0da2eb1df/blenderSMDexport3.jpg" alt="blenderSMDexport3" width=350 /><br/><br/><img src="uploads/2c929b6993d5e370c1d5498a03cb4ba8/blenderSMDexport4.jpg" alt="blenderSMDexport4" width=350 />
<br/><br/>

- For `g_*` and `v_*`:<br/>
Just select the mesh:
<br/><img src="uploads/ec37639792378bb6ef9684b19d45a056/blenderSMDexport5.jpg" alt="blenderSMDexport5" width=350 /><br/>

Use `iqm.exe` from [IQM exporter](https://gitlab.com/xonotic/iqm), in Windows, in CMD execute for convert SMD to IQM for `v_*` and `g_*`: <br/>`iqm v_myweapon.iqm v_mesh.smd`
<br/>`iqm g_myweapon.iqm g_mesh.smd`
<br/><br/>Linux: `./iqm.exe g_myweapon.iqm g_mesh.smd`
<br/>`./iqm.exe v_myweapon.iqm v_mesh.smd`

Rename extension to MD3: <br/>`g_myweapon.iqm` to `g_myweapon.md3`<br/>and `v_myweapon.iqm` to `v_myweapon.md3`

Use dpmodel tool to generate `h_myweapon.dpm` and `h_myweapon.dpm.framegroups`, in command line execute: `dpmodel yourconfig.txt`
<br/>More info explained about dpmodel tool: [dpmodel](dpmodel)
<br/>Rename extension to IQM: `h_myweapon.dpm` to `h_myweapon.iqm` and `h_myweapon.dpm.framegroups` to `h_myweapon.iqm.framegroups`.

Save those compiled model files into `models/weapons/` folder where they belong.

**Conclusion**: Sometimes, renaming formats can be useful to save effort. Note: it's a kind of weird way to do stuff, file formats are hardcoded in the gamecode rather than dynamically obtained. The issue of model resources was found here: https://gitlab.com/xonotic/xonotic-data.pk3dir/-/issues/2629#note_686988936

# Export troubleshooting

- _**Spamming CSQC errors:**_<br/>
If you received spamming errors like that:<br/>
<img src="uploads/1f769ede89975be78d88eaad1b062a3d/spammingerrorstagweapon.jpg" alt="spammingerrorstagweapon" width=450 /><br/>
It's because you didn't do correctly the steps after exporting the models. Remember, `g_*` and `v_*` MUST contain one bone. 

- _**Broken shadow artifacts (enabled with `r_shadows 2` and disabled `r_shadow_shadowmapping 0`):**_<br/>
To solve this, the possible way is exporting to IQM from Blender, selecting bone and mesh (both highlighted). Usually, this issue happens in `g_*` and `v_*` ones.
<br/>Reference: https://gitlab.com/xonotic/xonotic-data.pk3dir/-/issues/2667

- _**When exporting to IQM, get duplicated names in the texture:**_<br/> 
<img src="uploads/cdb90908aab4a52580019c871a936156/duplicatedtexturenameloadingerror.jpg" alt="duplicatedtexturenameloadingerror" width=700 /><br/>
Take a closer look what is telling here:<br/>
_The "Materials" option controls how material names are generated. "material+image-ext" combines the name of the material and the name of the UV-map image (without its extension or directory) to generate the material name, "material" uses only the name of the material, and "image" just uses the name of the UV-map image (without its directory)._ <br/><br/>
When you export to IQM, you need to watch out that detail after selected File > Export > Inter-Quake Model (.iqm, .iqe).<br/>
Pay attention to this [section](##textures-and-materials) when you need to export correctly, the material/texture(image) should be one thing, so do the following: Export IQM > Materials: > select `material` or `image` instead `material+image-ext`.<br/>
<img src="uploads/4fa75dca8ebb724b89d5fd5513fd2787/exportiqmiqeblenderdetails.jpg" alt="exportiqmiqeblenderdetails" width=200 /><br/>

- _**Model scale animations aren't working:**_<br/> 
SMD format doesn't support bone scaling animations, although you try to compile them into dpmodel tool, it won't change nothing. More info here: https://steamcommunity.com/groups/BlenderSourceTools/discussions/1/152392786900223348/ <br/>
Same goes for IQM.

# Old methodology

*Note: this section is obsolete.*

**IMPORTANT**: In this methodology, you can't make cool animations with separated objects, because `h_*.iqm` here is a plane mesh with bones.

Note: the model should be rotated as YXZ, instead being XYZ. Because the tip of the weapon is Y, horizontally is X.

`tag_weapon` is a MUST bone for this methodology.

## MD3

Before to export, you need to select all objects in the scene. (Pressing A, seeing all objects highlighting orange or red)

<img src="uploads/f3f39c9a06a408bffd5e48bc5e07eac6/exporttutorialMD3.jpg" alt="exporttutorialMD3" />

And export the model to MD3. _Attention: You can't export a model over than 8192 triangles/polys._

## IQM

Before to export, you need to select all objects and bones in the scene. (Pressing A, seeing all objects highlighting orange or red)

<img src="uploads/7df860d73a6fae27408c8f0c024122c7/exporttutorialIQM.jpg" alt="exporttutorialIQM" />

<br/>
<br/>

For `h_*.iqm`, you will need to make a simple plane with a scale reduced to 0.01 and the following bones attached: `tag_weapon`, `tag_shell` and `tag_shot`.

There's a post from InsideQC Forums explaining about IQM:

There is a simple way:

You just can use an iqm model like a regular mdl file, you only need to use each frame as a framegroup.

For example, if you have an iqm called "rocketlauncher.iqm" with 20 frames you need to create a framegroup file with notepad like this:

```
1 1 1 0  
2 1 1 0  
3 1 1 0  
4 1 1 0  
5 1 1 0  
6 1 1 0  
7 1 1 0  
8 1 1 0  
9 1 1 0  
10 1 1 0  
11 1 1 0  
12 1 1 0  
13 1 1 0  
14 1 1 0  
15 1 1 0  
16 1 1 0  
17 1 1 0  
18 1 1 0  
19 1 1 0  
20 1 1 0  
```
So you will save it with the name.

"rocketlauncher.iqm.framegroups" and place it in the same directory where "rocketlauncher.iqm" is located.

About the framegroups file (explained on [Framegroups](framegroups)):

The first number represents the start frame of the animation (.framegroups files always start at frame 0, some modeling software starts at 1, so you might have to subtract 1 for the start frame).

The second number represents how many frames is in that specific frame group.

The third number is the frame rate at which the sequence plays. This is independent from what the monsters "think" rate is, which I have set at 10 frames-per-second.

The fourth number is whether the frame sequence loops. 1 for true, 0 for false. The only frame groups that should loop are the standing, walking and running animations.

It becomes very tedious to make framegroups for models with really many frames, you can use this sample generated for the DarkPlaces model compiler. It will work in every iqm model (if you wan you only need to cut the specific number of frames in the framegroups file, but it will work anyway)

Reference: [IQM animation framegroups (InsideQC Forums)](https://forums.insideqc.com/viewtopic.php?p=55949&sid=952cd347938ae5f2bb8bde276b5a35cd#p55949)

