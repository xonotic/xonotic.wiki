# Murder in Megaerebus Manor (MMM)

<img src="uploads/c6e610dae225d4b3b3b6d983fbe4a1ee/gametype_mmm.png" alt="logoMMMxonoticluma"/>

<img src="uploads/4b0977974e9cd4db5f2e53b6e6a82e66/gametype_mmmluminos.png" alt="gametype_mmmluminos"/>

<img src="uploads/c2c9f4c388ac01ae9213c577c1afe7c3/gametype_mmmxaw.png" alt="gametype_mmmxaw"/>

## A group of space civilians have murderers among them. Murderers must kill civilians, while the civilians have to try to find and kill the murderers.

# Objective of the Game

The game mode is based on the fact that there is a small group of Murderers and another of Detectives, there is an explanation of the functions of each one.


## Civilians

The civilians are the majority (62.5% of the game's population by default.) Their group color is green.
The task of the civilian is to detect Murderers and defend detectives more than anything, they must kill the right person or anything special.


## Murderers

The Murderers are the medium sized group in MMM (25% of the population or one Murderer for every four players.) Their group color is red, but this is not displayed to anyone other than Murderers, or when a Murderer has been confirmed to be dead.
The Murderers must murder everyone undetected, they know who they are because only among themselves do they see a red "circle" that blinks around their companions' bodies. They must kill all Civilians before time runs out.


## Detectives

The Detectives are the smallest group in MMM (12.5% of the population or one Detective for every eight players.) They are special Civilians given equipment. Their group color is blue.
Detectives have special methods to detect the Murderer, for example, when they see a body, they must analyze the body, when seeing it, shoot with Shockwave weapon to the body, if the body was recently killed, it will tell if he was Civilian, Detective or a Murderer.


## About Karma points

It is worth clarifying that everyone can kill each other, but there is something called "**Karma**", which starts with an initial, and increases if they do things right (if traitors kill, or if traitors kill people), otherwise, the Karma will be discounted.

The Karma is used for the damage that weapons generate, the more Karma, the more damage your weapon will do. When Karma is lower, lesser damage your weapon will do.

If the Karma is lower, the player can be banned temporarily to avoid teamkills or any abuse with the end to keep the game in balance. In that case, the ban tool cvar is set to be forced to spectate by default.



# Screenshots

- Gamemode selection menu:

<img src="uploads/a1525b91e1519d2786431a2e5b43e69b/MenuMMM.jpg" alt="MenuMMM" width="400"/>

<br/>
<br/>
<br/>
<br/>

- Gameplay:

<img src="uploads/24d3a6caec031b68d70856f62982d96c/gameplayCivilian.jpg" alt="gameplayMMM" width="400"/>

<br/>
<br/>
<br/>
<br/>

- Detective role, when you use Shockwave weapon shooting dead players to see who killed them:

<img src="uploads/05a2c41d47a8767fe2f2e23943a8c210/detectiveUsingShockwave.jpg" alt="detectiveUsingShockwave" width="400"/>

<img src="uploads/46162b4daff7255e5eab7637665426a4/detectiveUsingShockwaveSlime.jpg" alt="detectiveUsingShockwaveSlime" width="400"/>

<br/>
<br/>
<br/>
<br/>

- Karma score, plus end of the round, everyone show their role, Murderers are revealed when round is over:

<img src="uploads/b9dd72b8571010e06ba5ee3d0be68845/karmaSystem.jpg" alt="karmaScore" width="400"/>

<br/>
<br/>
<br/>


# Rules

## Basic Rules

All basic rules apply to any role and or players.
​

- No Randomly Damaging Mate​

1. Randomly Damaging a teammate without sufficient evidence of them committing a kill on site as offence, This includes damages that have been done unintentionally.​

2. Killing any player that is away from their keyboard.​


- No Murderer Baiting

1. Performing Murdererous acts as civilian repeatedly.

2. Tricking other players into believing you're a Murderer.



## Role Rules

These rules only apply if you are apart of that role for example you can't break a detective rule if you're an civilian.​


<span style="color:green">**Civilians**</span>​

1. Do not commit any Murdererous acts.

2. Do not work with Murderers

3. Only kill when you have a valid reason to do so.

4. Obey valid detective orders at all times.

5. Do not harm the detectives in any way.

6. Do not use a Murderer defib at all.


<span style="color:red">**Murderers**</span>

1. Do not kill other Murderers.

2. Always try to alert other Murderers to your traps.

3. Do not sell out other Murderers.

4. Do not use a Cleaning defib at all.


<span style="color:blue">**Detectives**</span>

1. Do not give unreasonable orders.

2. Do not harm Civilians

3. Do not abuse your powers.

4. Do not work with Murderers.

5. Do not use the Murderer defib at All.

## Kill on Site Rules

A kill on site must come under valid for you to act on it. Any kill on site marked with the role letter and color (<span style="color:blue">D</span>, <span style="color:red">M</span> or <span style="color:green">C</span>) are only valid if given by that role. if you kill on site someone for an invalid reason then you may be warned for False kill on site.​

- Valid kill on site​

1. Self Defence.

2. Damage without a valid reason.​

3. Mass killing.​

4. Associating with Murderers.​

5. Hiding game sensitive items.​

6. Using Murderer weaponry without being proven.​

7. Failing to Identify bodies after killing.​

8. Walking past multiple unidentified bodies.​


- Invalid kill on site​

1. Kill on site on a Weapon.​

2. Kill on site on a Location.​

3. Not following Civilians orders.​

4. Suspicion.​

5. Aiming at players.​


## Notes for developers / mappers

- For developers, the source code is here:

[LegendGuard's MMM repository](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/LegendaryGuard/ttt)

- For mappers:

Inside the map (.pk3 file), in .mapinfo file, you write: `gametype mmm`

Moreover, you can enable this gamemode in the console using this command: 
`sv_cmd gametype mmm`

- Cvars:

`g_mmm_not_lms_maps 0` // "when this is set, LMS maps will NOT be listed in mmm"

`g_mmm_not_dm_maps 0` // "when this is set, DM maps will NOT be listed in mmm"

`g_mmm_murderer_count 0.25` // "number of players who will become murderers, between 0 and 0.9 to use a multiplier of the current players, or 1 and above to specify an exact number of players"

`g_mmm_detective_count 0.125` // "number of players who will become detectives, between 0 and 0.9 to use a multiplier of the current players, or 1 and above, to specify an exact number of players. 0 = no detectives"

`g_mmm_punish_teamkill 0` // "enable option to kill the player when they kill an ally"

`g_mmm_reward_civilian 1` // "give a point to all civilian players if the round timelimit is reached, in addition to the points given for kills"

`g_mmm_warmup 10` // "how long the players will have time to run around the map before the round starts"

`g_mmm_round_timelimit 180` // "round time limit in seconds"

`g_mmm_max_karma_points 1000` // "limit of maximum number of karma points will have in the server"

`g_mmm_min_karma_points 550` // "limit where number of karma points can be reached when are being decreased"

`g_mmm_karma_bankick_tool 1` // "tool for strict rules when karma is low: '0' nothing does, '1' forces player to spec, '2' kicks player, '3' bans player"

`g_mmm_karma_bantime 1800` // "number of seconds to ban someone with very low karma"

`g_mmm_karma_damageactive 1` // "enable karma damage rule. If a player's karma is low, they will not do as much damage as a player who has high or full karma"

`g_mmm_karma_damagepunishmentdeal 20` // "punishment damage points when player kills an ally"

`g_mmm_karma_severity 0.25` // "how severe karma is to decrease karma points to the players [0.1 - 1.0]"

`g_mmm_reward_detective 1` // "give a point to all detective players if investigated corpses"

<br/>
<br/>
<br/>
<br/>
<br/>

~~Previously and similarly, there was someone else who used to work on that kind of project, but it was separate and not entirely open. Data is from 2019.~~

~~[obsolete reference](https://gitlab.com/xonotic/xonotic/-/wikis/Trouble-in-Terrorist-Town)~~