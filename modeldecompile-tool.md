`modeldecompile` is a powerful command that decompiles any model format (DPM, MDL, IQM, MD3, BSP, etc...) to OBJ. It's available in any Darkplaces game including Xonotic and can decompile any model (weapon, player or any object/entity) present in the game filesystem (in the game installation directory and in the `data` directory inside [your user directory](http://www.xonotic.org/faq/#config).

Usage example: `modeldecompile models/weapons/v_crylink.md3`

It decompiles the mesh and the animations of a model in one action (the animations aren't distributed per actions).

An example of a .framegroups file from a player model (`okrobot1.dpm.framegroups`):

```
1 41 30 0 // okrobot1 die1
67 56 30 0 // okrobot1 die2
133 11 15 1 // okrobot1 draw
144 6 10 1 // okrobot1 duck
150 17 15 1 // okrobot1 duckwalk
167 31 40 0 // okrobot1 duckjump
198 1 10 1 // okrobot1 duckidle
199 101 15 1 // okrobot1 idle
300 17 20 1 // okrobot1 jump
317 6 10 0 // okrobot1 pain1
323 6 10 0 // okrobot1 pain2
329 6 30 0 // okrobot1 shoot
335 17 15 1 // okrobot1 taunt
352 17 30 1 // okrobot1 run
369 17 30 1 // okrobot1 runbackwards
386 17 30 1 // okrobot1 strafeleft
403 17 30 1 // okrobot1 straferight
41 1 10 0 // okrobot4 dead1
122 1 10 0 // okrobot4 dead2
424 17 30 1 // okrobot1 forwardright
441 17 30 1 // okrobot1 forwardleft
458 17 30 1 // okrobot1 backright
475 17 30 1 // okrobot1 backleft
492 21 15 1 // okrobot1 melee
```

The hacky way to obtain all animations of a model is setting the last keyframe in an exaggerated value (don't care if the fps have that value or aren't loopable) of the .framegroups file, the file must contain ONLY **one line**:

```
1 9999 30 0 // LegendGuard's legendary method
```

What that legendary method does is setting the initial keyframe to `1` and `9999` the last keyframe, we can know `9999` doesn't exist but reaches into a limit. (ignore `30` (means fps) and `0` (means nonloop, if `1` means loopable)).
In the game, execute `modeldecompile any_model.dpm` (any_model.iqm, any_model.md3, any_model.mdl, ...).

<img src="uploads/6140abcafb7b60cb154b43ce5e835105/image.png" alt="model_decompile_okrobot1_xon" width=760 />
<br/>

After that, go to the data directory where the files were decompiled.
Use the SMD files to import into [Blender](https://www.blender.org/) using [Blender Source Tools](http://steamreview.org/BlenderSourceTools/) addon.

First, import the mesh (`ref1.smd`) with the Bone Append Mode set to "Make New Armature" and "Import Animations" option disabled. <br/>
After, select the armature of the imported model (orange highlighted) and import other SMD file (`groupified_0_anim.smd`), before that, set the Bone Append Mode to "Validate Against Target" and enable "Import Animations" option. The animations in one action are applied to the armature.

Result (video): https://streamable.com/1egthv 

The actions can be created with the imported one to separate the keyframes of every animation.