##### Quick Resources

[Compiling release builds](Compiling)  
[Repository Access and git builds](Repository_Access)  
[Introduction to QuakeC](Introduction-to-QuakeC)  
[Tips for new developers](Programming-Tips)  
[Programming QuakeC stuff in Xonotic](Programming-QuakeC-stuff-in-Xonotic)  
