This mod is in use on the Freesh Meat servers, maintained by h0tc0d3, and contains the following modifications:

- Added voting for game type modifiers.
    ![Freesh Meat Club Game Mods](https://freshmeat.fun/gamemod/gamemods.jpg)
  - [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/63bbaec218093e16509de0ad799151a337bc2acf](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/63bbaec218093e16509de0ad799151a337bc2acf)

    - <img src="https://freshmeat.fun/gamemod/arc.png" alt="arc" width="64" height="64" /> Replace Machinegun with Arc(Shaft in Quake)
    - <img src="https://freshmeat.fun/gamemod/allweapons.png" alt="arc" width="64" height="64" /> Infinity Weapons
    - <img src="https://freshmeat.fun/gamemod/superweapon.png" alt="arc" width="64" height="64" /> Super Weapon
    - <img src="https://freshmeat.fun/gamemod/overkill.png" alt="Overkill" width="64" height="64" /> [Overkill](https://xonotic.org/okguide/)
    - <img src="https://freshmeat.fun/gamemod/nade.png" alt="Nade" width="64" height="64" /> [Nades](https://gitlab.com/xonotic/xonotic/-/wikis/Nades)
    - <img src="https://freshmeat.fun/gamemod/nix.png" alt="NIX" width="64" height="64" /> [NIX - Random Weapon Mod](https://gitlab.com/xonotic/xonotic/-/wikis/NIX)
    - <img src="https://freshmeat.fun/gamemod/skill.png" alt="Skill" width="64" height="64" /> Skill - Reduce hagar and electro power. Which reduces the noskill advantage.
      - [https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/1057](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/1057)
      - [https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/1056](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/1056)
    - <img src="https://freshmeat.fun/gamemod/powerup.png" alt="Powerup" width="64" height="64" /> Powerups in game
    - <img src="https://freshmeat.fun/gamemod/buff.png" alt="Buff" width="64" height="64" /> Buffs in game
    - <img src="https://freshmeat.fun/gamemod/hook.png" alt="Grappling Hook" width="64" height="64" /> [Grappling Hook](https://gitlab.com/xonotic/xonotic/-/wikis/Grappling-Hook)
    - <img src="https://freshmeat.fun/gamemod/xpm.png" alt="Xonotic PRO Mode" width="64" height="64" /> [Xonotic PRO Mode](https://xonotic.fandom.com/wiki/Mods)
    - <img src="https://freshmeat.fun/gamemod/instagib.png" alt="Instagib" width="64" height="64" /> [Instagib](https://gitlab.com/xonotic/xonotic/-/wikis/InstaGib)

- Added random movement and dodging for the bot. Which makes the game more like playing with a human. The bot also moves more left-right than back and forth to make it more like a strafe. [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/b2c1e5641fd45e2dad12fb422c85289b195e70bc](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/b2c1e5641fd45e2dad12fb422c85289b195e70bc)

- Added hats for players models
  - [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/92b99b188640c63d3e934a22dc5fa2f06739e1ba](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/92b99b188640c63d3e934a22dc5fa2f06739e1ba)

- Added improved weapon effects. Allows you to better dodge and direct missiles. Enable: cl_particles_pro 1
  - [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/d42f9d1da8298465ba72dec11ad5bff6ef61937b](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/d42f9d1da8298465ba72dec11ad5bff6ef61937b)

- Improved voting window for maps and game type. Decrease title font size and raise it higher. The title looked disproportionate and wasted usable space. Solve the problem of incorrectly cropped text.
  - [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/f4aca361e835a9e49673afb5427a70297fc34185](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/f4aca361e835a9e49673afb5427a70297fc34185)

- Improved scoreboard. The game type font is too big and looks disproportionate. Most of the information did not fit on the screen. Brings the scoreboard to a more familiar look like in other games.
  - [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/5289a443562381f7c8a03e95df438bf702c9710e](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commit/5289a443562381f7c8a03e95df438bf702c9710e)


Complete source code (except possibly for server-only changes that do not affect csprogs.dat or other code sent to clients) to be found on [https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commits/archlinuxclub/](https://gitlab.com/h0tc0d3/xonotic-data.pk3dir/-/commits/archlinuxclub/)

Build command:

`make qc QCCFLAGS_WATERMARK=xonotic-v0.8.5_r9-votpidor-edition`

Icons used:
- https://www.flaticon.com/authors/amethystdesign
- https://www.flaticon.com/authors/justicon
- https://www.flaticon.com/authors/febrian-hidayat
- https://www.flaticon.com/authors/smalllikeart
- https://www.flaticon.com/authors/those-icons
- https://www.flaticon.com/authors/flatart-icons
- https://www.freepik.com/
- https://flat-icons.com/
