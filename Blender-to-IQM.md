Blender to IQM
==============

Blender when exporting to IQM
-----------------------------

### Needed/Example files

* Bounding Box for Stand and Crouch: [bbox_crouch.blend](assets/files/bbox_crouch_stand/bbox_crouch.blend), [bbox_standing.blend](assets/files/bbox_crouch_stand/bbox_standing.blend)
* Erebus example blend: [erebus.blend](https://gitlab.com/xonotic/mediasource/raw/master/models/player/erebus.blend)
* Erebus example framegroups: [erebus.iqm.framegroups](https://gitlab.com/xonotic/xonotic-data.pk3dir/raw/master/models/player/erebus.iqm.framegroups)
* Link to exporter: http://sauerbraten.org/iqm/

### Before You Start

* use the bbox.blend in order to see the size that your model should be. Iqm exporter scale function untested.
* use the duck_bbox.blend in order to see the size that your model should be when crouching.
* for more information on bbox size refer to div0’s model specs [Player_Model_Spec](Player-Model-Spec)
* naming conventions for textures and model go as follow assuming the example is the umbra model: modelname: umbra.iqm, framegroups: umbra.iqm.framegroups, textures: umbra.tga or (jpg), umbra_norm.tga, umbra_gloss.tga, (need to add more)

### Exporting

1. Select both mesh and armature in object mode.
2. Go to scripts \> export \> inter-quake-model.
3. Now make sure bounding boxes and meshes are both highlighted in the script window.
4. Export to a file path of your choosing, /path/to/umbra.iqm. Put the names of all the animations in the animations box, using commas for multiple anims. Then hit the export button and hopefully it will work without any errors.
5. Your model will need a .framegroups file to be used in Xonotic, look at the example file at the beginning of this article or check the [Framegroups](Framegroups) section of this wiki.

### Troubleshooting

- _**Broken shadow artifacts (enabled with `r_shadows 2` and disabled `r_shadow_shadowmapping 0`):**_<br/>
To solve this, the possible way is exporting to IQM from Blender, selecting bone and mesh (both highlighted). That issue could happen to player models.
<br/>Reference: https://gitlab.com/xonotic/xonotic-data.pk3dir/-/issues/2667

- _**When exporting to IQM, get duplicated names in the texture:**_<br/> 
<img src="uploads/cdb90908aab4a52580019c871a936156/duplicatedtexturenameloadingerror.jpg" alt="duplicatedtexturenameloadingerror" width=700 /><br/>
Take a closer look what is telling here:<br/>
_The "Materials" option controls how material names are generated. "material+image-ext" combines the name of the material and the name of the UV-map image (without its extension or directory) to generate the material name, "material" uses only the name of the material, and "image" just uses the name of the UV-map image (without its directory)._ <br/><br/>
When you export to IQM, you need to watch out that detail after selected File > Export > Inter-Quake Model (.iqm, .iqe).<br/>
Pay attention to this [section](##textures-and-materials) when you need to export correctly, the material/texture(image) should be one thing, so do the following: Export IQM > Materials: > select `material` or `image` instead `material+image-ext`.<br/>
<img src="uploads/4fa75dca8ebb724b89d5fd5513fd2787/exportiqmiqeblenderdetails.jpg" alt="exportiqmiqeblenderdetails" width=200 /><br/>

- _**Model scale animations aren't working:**_<br/> 
IQM and SMD formats don't support bone scaling animations, although you try to compile them into dpmodel tool, it won't change nothing. More info here: https://steamcommunity.com/groups/BlenderSourceTools/discussions/1/152392786900223348/ <br/>

### Notes

As of 01/06/2012, Xonotic uses these animations:

    dieone,dietwo,draw,duck,duckwalk,duckjump,duckidle,idle,jump,painone,paintwo,shoot,taunt,run,runbackwards,strafeleft,straferight,deadone,deadtwo,forwardright,forwardleft,backright,backleft,melee,duckwalkbackwards,duckstrafeleft,duckstraferight,duckforwardright,duckwalkforwardleft,duckbackwardright,duckbackwardleft
