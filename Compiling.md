---
title: Compiling Xonotic releases and autobuilds
---

This page is about compiling the sources included in [stable releases](https://xonotic.org/download) and [beta autobuilds](Autobuilds), which you may want to do after [updating by rsync](https://xonotic.org/download#upgrading).  
For git builds and development tools please see [Repository_Access](Repository_Access) and [Git HOWTO](Git).

The release Makefile is the most convenient way to compile for alternative CPU architectures, or to get a build fully optimised for a recent x86 CPU (the official binaries are generic x86_64).  It would also allow you to run Xonotic on an OS too old to be supported by official binaries.

To this end the Makefile default settings enable all optimisations that can be used on your current CPU without causing bugs, and compile using all available CPU threads.

The engine builds will be dynamically linked except for d0_blind_id which is statically linked to ensure reliable player ID support and because OS distributions don't ship this library.

### Commands

Invoking `make` or `make help` in the Xonotic directory will print the supported targets and current config.  

**Most people will want `make update-stable client` or `make update-beta client`**

## System package dependencies
### Debian
Build (client): `build-essential automake libtool libgmp-dev libjpeg-dev libsdl2-dev`  
Build (server): `build-essential automake libtool libgmp-dev libjpeg-dev zlib1g-dev`  
Runtime (client): `curl rsync libpng16-16 libfreetype6 libvorbisfile3`  
Runtime (server): `curl rsync libpng16-16`  
