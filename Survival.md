Survival
===============

Object of the Game
------------------

Survival is a round-based game with 2 teams: Hunters and Survivors. But there's a twist: Players get randomly assigned a team and you only know your own team membership, but not to which team everyone else belongs.

The goal of the hunters is to kill all the survivers.
The goal of the survivors is to either kill all the hunters or to survive the round (until the round timer hits 0).

Each player will individually score during the rounds and the player with the most points at the end wins the whole match.


Scoring
-------

| Action | Score |
|:-------|------:|
| Survive the round | +1 for each opponent you killed in that round |
| Survive the round by letting the round timer run out (Survivors only) | +1 |
| Kill a team mate | -1 |
| Kill yourself | -1 |

Note: These are only defaults: Servers can modify the score if they want.

Details
-------

Each round begins with a brief warmup period where players can't shoot and they don't know their role (Hunter/Surviver) yet but they can already walk around and collect goodies. Once the warmup period is over, the weapons unlock, your role is revealed and the action begins.

By default, the hunter team is smaller than the surviver team (ca. 25% of all players).

Think before you shoot! If you accidentally kill a team mate, you will be punished with death as well and are eliminated from the round.

Once a round is over, the role of each player is revealed: Survivors will be in green while hunters are red. You can also look it up in the scoreboard.