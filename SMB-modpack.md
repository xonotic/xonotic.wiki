Developed and maintained by **[Mario](https://github.com/MarioSMB)** and **[TimePath](https://github.com/TimePath)**.

The **[modpack](https://github.com/MarioSMB/modpack)** was **[TimePath](https://github.com/TimePath)**'s experiment to split "custom" features into a secluded codebase. <br/>
It makes it used to create games based on the Xonotic template.

The modpack build works using a submodule from '_xonotic-data.pk3dir_' from the official gamelogic development repository.

Uses **update.sh** and **build.sh** shell script files. 

### You can create mods from the modpack like: 
### **[Quake](https://github.com/MarioSMB/modpack/tree/quake)**
<img src="uploads/a622ef2d5c0c5ca81d6f044c452293eb/quake_mod_sample.jpg" alt="quake_mod_sample" width=550 />

### **[Prydon Gate](https://github.com/MarioSMB/modpack/tree/prydon)**
<img src="uploads/2dd93a2f8ce7b22400c32535c9fad392/prydon_mod_sample.jpg" alt="prydon_mod_sample" width=550 />

### **[Suwa Moriya 64](https://github.com/MarioSMB/modpack/tree/moriya)**
<img src="uploads/33a69c4499702fbf8592766ebae04c34/suwa_moriya_64_mod_sample.jpg" alt="suwa_moriya_64_mod_sample" width=550 />

# How submodule works, update.sh and build.sh

Uses '_update.sh_' to obtain the content of '_gmqcc_', '_xonotic-data.pk3dir_' in '_xonotic_' folder and '_csprogs_' in '_.cache_' folder. Clearly, needs gmqcc to be able to compile, this means that '_build.sh_' calls gmqcc to compile the code.

## Submodule and update.sh

The submodule of this modpack was created using: 
```sh
git submodule add https://gitlab.com/xonotic/xonotic-data.pk3dir xonotic
```

The submodule in the repository is used when need to update the code from '_xonotic-data.pk3dir_', to do this if you've a fork or a copy of SMB modpack repository, you'll have to update using (note: in your local computer, you should have used '_update.sh_' to download fully the content of '_xonotic-data.pk3dir_' in '_xonotic_' folder):
```sh
git submodule update --remote --merge
```

It'll update the content where is in the official gamelogic development repository in your modpack repository.

Inside '_update.sh_', there is a line: 
```sh
git submodule update --init --depth 100
```

Means that when you use '_update.sh_' with this line, the submodule content will be downloaded in your local computer where you've cloned the modpack repository.

To run '_update.sh_' script, you should go to the current directory where is this script and execute:
```sh
./update.sh
```

### Commit with submodule

Additionally, after done `git submodule update --remote --merge`, if you want to commit:
```sh
git status
git add xonotic
git commit -m "Update submodule xonotic test"
git push origin master #depends what origin and what branch of repository you're using to do your commits
```
## build.sh

'_build.sh_' compiles all repository code using gmqcc if already downloaded from '_update.sh_', looks inside '_mod_' folder the similar contents of '_xonotic-data.pk3dir/qcsrc_' where there are: '_client_', '_common_', '_menu_' and '_server_' folders.

What this does is relate the directory path to the submodule '_xonotic_' directory inside the '_qcsrc_' folder. Thanks to this, makes possible to look at the '_mod_' folder having to relate the submodule directory '_xonotic/qcsrc_'.

To run '_build.sh_' script, you should go to the current directory where is this script and execute:
```sh
./build.sh
```

For speed up compilation, you can run with:
```sh
ZIP=: ./build.sh
```

That executes without compression (without getting `csprogs-*.pk3` file).