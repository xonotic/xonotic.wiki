---
title: Xonotic Autobuilds
---

Autobuilds are "beta" builds, like [stable release](https://xonotic.org/download/) builds but with all the latest changes from git.  
They're the easy way to play or host the latest development version. Like stable releases they include source code and an easy way to [compile](Compiling) it for various CPUs.  

If you "installed" Xonotic by unpacking the stable release zip you can update it to the latest autobuild using the scripts located in `misc/tools/rsync-updater` (use `update-to-autobuild.bat` if you're on Windows, `update-to-autobuild.sh` on Linux and Mac). You can also [download an autobuild zip](https://beta.xonotic.org/autobuild/).

It's best to **make a copy of the original Xonotic directory first** in case you wanna go back to using the release but you can also go back by using the `update-to-release` scripts. Releases and autobuilds should be able to coexist (they will share the same [config](https://www.xonotic.org/faq/#config) though, unless you use something like `-userdir`).

Autobuilds (and [git builds](Repository_Access)) are compatible with the same servers as the latest release. When you connect to a server, Xonotic downloads the gamelogic from it (and runs it sandboxed) so you always use the same version as the server. Autobuilds therefore "only" affect the gamelogic of local or hosted games, the engine (rendering, input, ...), assets (models, sounds, ...), menu, config and scripts.
