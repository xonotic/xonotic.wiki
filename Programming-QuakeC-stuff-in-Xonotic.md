_This article is written as developer notes to ease developer tasks and save QuakeC terms here._

# _Table of Contents_
> ### 1. [CSQC, MENUQC, SVQC and GAMEQC blocks](#csqc-menuqc-svqc-and-gameqc-blocks)
> ### 2. [MUTATOR functions](#mutator-functions)
> > 2.1. [How to use MUTATOR functions](#how-to-use-mutator-functions) <br/>
> > 2.2. [List of MUTATOR functions](#list-of-mutator-functions)
> ### 3. [Weapon functions](#weapon-functions)
> > 3.1. [A bit of introduction](#a-bit-of-introduction) <br/>
> > 3.2. [List of weapon functions](#list-of-weapon-functions)
> ### 4. [HUD, Menu and Draw functions](#hud-menu-and-draw-functions)
> ### 5. [Gamemodes](#gamemodes)
> > 5.1. [Samples](#samples)
> ### 6. [Effects](#effects)

# CSQC, MENUQC, SVQC and GAMEQC blocks

Xonotic uses the ***csprogs.dat*** from the server you're playing on, as it must match the server's ***progs.dat*** code (whereas your ***menu.dat*** is always your local copy).

Menu and HUD code are actually separated as they run in 2 distinct programs: ***csprogs.dat*** and ***menu.dat***

Server program is ***progs.dat***

- Code inside a **`#ifdef CSQC`** block is part of the client code and will be compiled to **csprogs.dat**

- Code inside a **`#ifdef MENUQC`** block is part of the menu code and will be compiled to **menu.dat**

- Code inside a **`#ifdef SVQC`** block is part of the menu code and will be compiled to **progs.dat**

- Also, code inside a **`#ifdef GAMEQC`** block is part of both client (not menu) and server code.

### A bit of explanation about the use of each type of cvars

- **g_*** cvars:

Example: `g_balance_grapplehook_speed_fly` is clearly a server or game cvar (**g_*** cvars are server and game cvars), so you CAN'T declare it within a `#ifdef CSQC` block. This type of cvar only can be declared inside a **`#ifdef SVQC`** or **`#ifdef GAMEQC`** block (if possible).

- **sv_*** cvars:

Example: `sv_campcheck` is clearly a server cvar (**sv_*** cvars are server cvars), only can be declared inside a **`#ifdef SVQC`** block.

- **cl_*** cvars:

Example: `cl_chatsound` is clearly a client cvar (**cl_*** cvars are client cvars), only can be declared inside a **`#ifdef CSQC`** block.

- **hud_*** cvars:

Example: `hud_progressbar_alpha` is a client cvar (**hud_*** cvars belong to client cvars), only can be declared inside a **`#ifdef CSQC`** block.


<br />

# MUTATOR functions

### How to use MUTATOR functions

You can look the following link if you're trying/testing your own mutator: [Writing your first mutator](writing-your-first-mutator)

**Attention:** to use a hook, first register your mutator using `REGISTER_MUTATOR`, then create your function using `MUTATOR_HOOKFUNCTION`.


- **`REGISTER_MUTATOR`**

Registers a new `MUTATOR_HOOKFUNCTION`.
```c
REGISTER_MUTATOR(new_mutator_name, some_variable);
```


- **`MUTATOR_HOOKFUNCTION`**

Creates a function and calls `new_mutator_name` (from `REGISTER_MUTATOR`) and one of `#define MUTATOR()` (from `qcsrc/server/mutators/events.qh` main hooks).

Example:
```c
MUTATOR_HOOKFUNCTION(new_mutator_name, events.qh_main_hook)
{
    // whatever does
}
```

- **`MUTATOR_CALLHOOK`**

Calls some `MUTATOR_HOOKFUNCTION`.
```c
MUTATOR_CALLHOOK(name_of_MUTATOR_HOOKFUNCTION_or_events.qh_main_hook);
```

There are different versions and depends how many variables need to be called in a `MUTATOR_HOOKFUNCTION`:

1 `MUTATOR_HOOKFUNCTION` and 1 variable:

```c
MUTATOR_CALLHOOK(name_of_MUTATOR_HOOKFUNCTION_or_events.qh_main_hook, some_variable);
```

1 `MUTATOR_HOOKFUNCTION` and 2 variables:

```c
MUTATOR_CALLHOOK(name_of_MUTATOR_HOOKFUNCTION_or_events.qh_main_hook, some_variable, some_variable);
```

1 `MUTATOR_HOOKFUNCTION` and 3 variables:

```c
MUTATOR_CALLHOOK(name_of_MUTATOR_HOOKFUNCTION_or_events.qh_main_hook, some_variable, some_variable, some_variable);
```

1 `MUTATOR_HOOKFUNCTION` and multiple variables:

```c
MUTATOR_CALLHOOK(name_of_MUTATOR_HOOKFUNCTION_or_events.qh_main_hook, some_variable, some_variable, some_variable, some_variable, ...);
```

<br />

## List of MUTATOR functions

You can look the MUTATOR functions in:

- **COMMON:** [`qcsrc/common/mutators/events.qh`](https://timepath.github.io/scratchspace/d4/d95/common_2mutators_2events_8qh.html)

- **CLIENT:** [`qcsrc/client/mutators/events.qh`](https://timepath.github.io/scratchspace/d8/d0e/client_2mutators_2events_8qh.html)

- **SERVER:** [`qcsrc/server/mutators/events.qh`](https://timepath.github.io/scratchspace/d6/ddd/server_2mutators_2events_8qh.html)

<br />
<br />

# Weapon functions

## A bit of introduction

You'll need understand that weapons can be interacted with **autocvars** variables (example: `float autocvar_g_balance_someweapon_speed;`) and each weapon with .qh file where cvars are declared without the need to declare using `autocvar_blabla...`. *Think*, *touch*, *explode* and those types of functions can be declared within an *attack* function and *whatever* it is called. Note that there are also METHODs that are necessary for the weapon to work.

- **Think** function makes the weapon shot react on what to do.
- **Touch** function makes the weapon shot react against some element where touch (like player, monster, vehicle, ...).
- **Explode** function makes the weapon explosion react.
- **Attack/Whatever** function is where the weapon attack executes, keep in mind that it has the other functions attached to be executed.

You can look the example of this weapon code:
- [**devastator.qh**](https://timepath.github.io/scratchspace/d9/dfa/devastator_8qh_source.html)
- [**devastator.qc**](https://timepath.github.io/scratchspace/d9/d5d/devastator_8qc_source.html)

<br />

## List of weapon functions

This is a created list of functions to modify/create weapons. There may be incomplete explanations for each function.

There are contents taken from [`qcsrc/common/weapons/all.qh`](https://timepath.github.io/scratchspace/d0/ddd/weapons_2all_8qh_source.html)

**WARNING:** The contents may content wonky code, and the interactions can change and not do the same what happens here.

<br />

- [**W_SetupShot_Dir**](https://timepath.github.io/scratchspace/d4/d3f/tracing_8qh.html#aff0ea351757ee6caf83b25d12d18656c)

```c
W_SetupShot_Dir(
	ent,
 	wepent,
 	s_forward,
 	antilag,
 	recoil,
 	snd,
 	chan,
 	maxdamage,
 	deathtype 
)
```

- [**W_SetupProjVelocity_Explicit**](https://timepath.github.io/scratchspace/d7/d31/tracing_8qc.html#a55f8f2b1828413bfb123a5fcb61b9f8e)

```c
void W_SetupProjVelocity_Explicit(
    entity 	proj,
    vector 	dir,
    vector 	upDir,
    float 	pSpeed,
    float 	pUpSpeed,
    float 	pZSpeed,
    float 	spread,
    float 	forceAbsolute 
)
```

- [**W_MuzzleFlash**](https://timepath.github.io/scratchspace/d0/ddd/weapons_2all_8qh_source.html)(located in **`qcsrc/common/weapons/all.qh`** line 406)

In the moment when player shots the weapon, weapon flashes. 

**Note:** write **`#ifdef SVQC`** at the start of using this function, and write with **`#endif`** after declared the function (only can do this if the code needs to be executed, although maybe you need more functions/things in the code inside this).

```c
void W_MuzzleFlash(Weapon thiswep, entity actor, .entity weaponentity, vector shotorg, vector shotdir);
```

- [**Weapon selection functions**](https://timepath.github.io/scratchspace/d8/d6b/selection_8qh.html)
(located in **`qcsrc/server/weapons/selection.qh`**)

- [**Weapon decrease ammo, speed factor, rate factor, reload functions**](https://timepath.github.io/scratchspace/d5/de0/weaponsystem_8qc.html)
(located in **`qcsrc/server/weapons/weaponsystem.qh`**)

<br />

# HUD, Menu and Draw functions

You can look the draw functions where HUD is being drawn: 
- [**Draw functions and macros (`qcsrc/client/draw.qh`)**](https://timepath.github.io/scratchspace/d5/d8d/client_2draw_8qh_source.html)

More info inside the code where you can draw/print HUD and menu: 
- [**Globals, constants, fonts, prints, error cmds, vector stuff, conversion functions and all drawing/printing HUDs/menus/characters related (`qcsrc/dpdefs/upstream/menudefs.qc`)**](https://timepath.github.io/scratchspace/d8/de2/menudefs_8qc_source.html)

Important changes about menus we could learn of:
- [**Terms of Service in window display and multiplayer dialog**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/commit/19bd3a4f)
- [**Welcome dialog**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/commit/19d753312f0deff7bd297822907874d532e9303e)
- [**A button to properly quit the campaign, a "Game menu" (on ESC) and remove the Disconnect dialogue**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/commit/2b2f743fc929bbbe0fc765edf8c433c946764607)
<br />

# Gamemodes

Keep in mind that to develop a gamemode, you need a map having enabled inside its *name_of_map.mapinfo* file this: `gametype shortcut_of_gamemode`. (example: `gametype tdm` (tdm is Team Deathmatch). After modifying the map file, compress the map contents, save as `.zip` and rename the extension to `.pk3`

Making gamemodes doesn't seem easy. There's a little explanation of what does each element. 

### Generated files for gamemodes:

[`qcsrc/common/gamemodes/gamemode/_mod.inc`](https://timepath.github.io/scratchspace/d9/d67/common_2gamemodes_2__mod_8inc_source.html) generated file where the includes of each gamemode are stored.

[`qcsrc/common/gamemodes/gamemode/_mod.qh`](https://timepath.github.io/scratchspace/d7/d0b/common_2gamemodes_2__mod_8qh_source.html) generated file where the includes of each gamemode are stored.

<br />

### Gamemode files:

- Server type:

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/sv_name_of_gamemode.qc`

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/sv_name_of_gamemode.qh`

<br />

- Client type:

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/cl_name_of_gamemode.qc`

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/cl_name_of_gamemode.qh`

<br />

- Base:

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/name_of_gamemode.qc`

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/name_of_gamemode.qh`

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/_mod.inc` generated file where the includes of a gamemode is generated.

`qcsrc/common/gamemodes/gamemode/name_of_gamemode/_mod.qh` generated file where the includes of a gamemode is generated.

<br />

### Optional things (if necessary):

[`qcsrc/common/ent_cs.qc`](https://timepath.github.io/scratchspace/d5/dff/ent__cs_8qc_source.html) (if an `ENTCS_PROP()` is needed for a gamemode)

[`qcsrc/common/mutators/mutator/waypoints/all.inc`](https://timepath.github.io/scratchspace/d2/d2b/mutators_2mutator_2waypoints_2all_8inc_source.html) where `REGISTER_WAYPOINT` can interact the situation of waypoints.

<br />

### Notifications, applying BADCVAR and displaying gamemode in the menu: 

[`qcsrc/common/notifications/all.inc`](https://timepath.github.io/scratchspace/d9/d09/notifications_2all_8inc_source.html) where notifications output appears in the scenario.

[`qcsrc/common/notifications/all.qh`](https://timepath.github.io/scratchspace/dc/d8f/notifications_2all_8qh_source.html) where `CASE(CPID, NAME_OF_ELEMENT)` is needed for a gamemode.

[`qcsrc/menu/xonotic/util.qc`](https://timepath.github.io/scratchspace/df/d9b/menu_2xonotic_2util_8qc_source.html) where gamemodes can appear in the main menu (Multiplayer > Create). `GAMETYPE(MAPINFO_TYPE_NAME_OF_GAMEMODE) \`
 
[`qcsrc/server/world.qc`](https://timepath.github.io/scratchspace/dd/dbe/world_8qc_source.html)  where `BADCVAR(g_*)` for each gamemode are stored.

<br />

### Configuration files where gamemodes interact:

`xonotic-data.pk3dir/gamemodes-server.cfg` where server gamestart hooks, gametype vote hooks, respawn delay/waves/weapon_stay cvars and gamemode game/server cvars connect.

`xonotic-data.pk3dir/gamemodes-client.cfg` where client gamestart hooks cvars connect.

`xonotic-data.pk3dir/notifications.cfg` where notifications need to be activated in the scenario.

<br />

### Menu and HUD icons:

`gfx/menu/luma/gametype_nameshortcut.tga`

`gfx/menu/luminos/gametype_nameshortcut.tga`

`gfx/menu/wickedx/gametype_nameshortcut.tga`

`gfx/menu/xaw/gametype_nameshortcut.tga`

(Example: `gametype_tdm.tga` is an icon of Team Deathmatch)

<br />

## Samples

There are good examples of the commits and repositories of people who developed gamemodes:

- [**Lyberta's GunGame gametype commits**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/482/diffs)

Taken from: https://forums.xonotic.org/showthread.php?tid=7314

You can look and test the [**Lyberta's GunGame gametype repository**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/Lyberta/GunGame)

- [**How Survival gamemode was developed (first commit)**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/commit/8f61a40877542ac94baa74c5ed653c77b77bd855)

It's recommended use the final product in this [**repository**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/Mario/survival). Many changes have been influenced.

[**SMB modpack repository**](https://github.com/MarioSMB/modpack/tree/master/mod/common/gamemodes/survival) has added Survival gamemode and others.

# Effects

Spawns effect particle in-game.

The effects can be used with `Send_Effect(effect, org, vel, howmany)` function to test the effects from [**qcsrc/common/effects/all.inc**](https://timepath.github.io/scratchspace/d9/d95/effects_2all_8inc_source.html)

**Send_Effect** function definition is inside: [**qcsrc/common/effects/effect.qh**](https://timepath.github.io/scratchspace/d5/de6/effect_8qh_source.html)

For example, you can test the smokes, using `EFFECT_SMOKE_LARGE`:
```c
Send_Effect(EFFECT_SMOKE_LARGE, this.origin, '0 0 0', 1);
```

EFFECT list: [**qcsrc/common/effects/all.inc**](https://timepath.github.io/scratchspace/d9/d95/effects_2all_8inc_source.html)

<br />