> ## Table of Contents
> 1. [Notes for developers](#notes-for-developers)
> 2. [HUD configuration](#hud-configuration)
> 3. [Chat sounds](#chat-sounds)
> 4. [Country flags](#country-flags)
> 5. [References](#references)


[**Mod**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/z411/bai-server) developed by [**z411**](https://gitlab.com/z411) for the BaI Xonotic server. Most of these are features ported from Quake 3/Quake Live and some might be undesirable for some Xonotic players, here is the content in case Xonotic players want any of them in the main game.


# Notes for developers

### Mod resources

Note: PATH is one of the following depending on your system

|OS|Path|
|:--|:--|
|Windows|C:\Users\yourusername\Saved Games\xonotic|
|Linux|~/.xonotic|
|Mac|~/Library/Application Support/xonotic|
Nothing should *ever* be added to the main installation folder of Xonotic, use these paths!

The resources should be saved in `PATH/data`.

- Announcer (execute `cl_announcer shobon` command to activate the resource):

[zzz-bai-announcer-v9.pk3](uploads/bc6f4532a636aae501c8a800ceed0eb2/zzz-bai-announcer-v9.pk3)

- Icons of country flags, medals and scoreboard logo:

[zzz-bai-gfx-v8.pk3](uploads/c9dd058351dd79b05014293df321836d/zzz-bai-gfx-v8.pk3)

- Soundtrack of when a match is over and the players are in pause voting to select gamemodes/maps:

[zzz-bai-jingle-v1.pk3](uploads/8a541fbdf6d2ab0447567eb1e18d3e13/zzz-bai-jingle-v1.pk3)

- Chat sounds:

[zzz-bai-jokes-v19.pk3](uploads/0f9c790bf32b98628b7eaf439cd72bc7/zzz-bai-jokes-v19.pk3)

- Quickmenu:

[zzz-bai-quickmenu-v2.pk3](uploads/7607735ad75ec0853a3f88a3ecf018ae/zzz-bai-quickmenu-v2.pk3)

- Set of sound effects about when player shots, a flag is fallen, ...

[zzz-bai-sounds-v11.pk3](uploads/247dc57e177a7d08c2b7ce7e42cd3995/zzz-bai-sounds-v11.pk3)

### Source code

The code is in z411's branch: https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/z411/bai-server

LegendGuard has forked the mod and added some stuff in this branch: https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/LegendaryGuard/bai_mod

Remember look the [**repository guide**](https://gitlab.com/xonotic/xonotic/-/wikis/Repository_Access) to use this code.

#### List of source code files where mod was developed

Client base:

- [`qcsrc/server/client.qc`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/server/client.qc)

Scoreboard:

- [`qcsrc/client/hud/panel/scoreboard.qc`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/client/hud/panel/scoreboard.qc)

- [`qcsrc/common/scores.qh`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/common/scores.qh)

Chat:

- [`qcsrc/client/hud/panel/chat.qc`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/client/hud/panel/chat.qc)

Spectator HUD:

- [`qcsrc/client/hud/panel/spect.qh`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/client/hud/panel/spect.qh)

- [`qcsrc/client/hud/panel/spect.qc`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/z411/bai-server/qcsrc/client/hud/panel/spect.qc)

Attacker text:

- All files inside this folder:
[`qcsrc/common/mutators/mutator/attackertext/`](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/z411/bai-server/qcsrc/common/mutators/mutator/attackertext)


# HUD configuration

Cvars: 

`hud_panel_spect_scores 1 //sets team spectator display HUD scores`

`hud_panel_spect_playername 1 //sets player name spectator display HUD`

<img src="uploads/2dc395d83ac793aa20f502a1bfd5bb12/SpectatorHUD.jpg" alt="SpectatorHUD" width="500"/>


`cl_attackertext 1 //sets attacker text to know who is attacking`

<img src="uploads/19d0104a69a9bf92aa0b65c111ab0488/attackername.jpg" alt="attackername" width="200"/>


`hud_panel_itempickup 1 //sets item pickup entire display HUD`

`hud_panel_itempickup_timer 1 //sets item pickup timer display`

<img src="uploads/3d3d116722312d471408c7d178595e6d/items.jpg" alt="items" width="200"/>

# Chat sounds

To activate chat sounds, you can add sounds as you want:
You should put the sounds inside a .pk3 in the path `sound/chat/sound_name.ogg`
For example:
```
sound/chat/sound1.ogg
sound/chat/sound2.ogg
sound/chat/sound3.ogg
```
Then you should set the following cvars in your server config:
```
set sv_chat_sounds 1
set sv_chat_sounds_list "sound1 sound2 sound3"
```
The cvar `cl_chat_sounds` is only on the client side in case you're on a server with sounds and these bother you, you can disable them from your side only.

BaI server uses the following chat sounds configuration (chat sounds pk3 resource is needed to use these sounds):
```
cmd sounds //to check chat sound list
set sv_chat_sounds 1
set sv_chat_sounds_list "200 abusadores alert andate antabaka apruebo arigato au avispate aweonao batman bonk bork brigido bruh buitre callese chan chaojefe chimp choro choro2 choro3 combobreaker cristian ctm ctm2 ctm3 cyka daleboludo drop drop2 economia eliminado enfermedad fama felipe fumando1 fumando2 gallina garrotera gas gay gracias hitler homero israel jiji july3p lacra laputaquemepario larva lepego macaco matias mefunaron meopene meperdonas mequierematar mevoy mipixula mira mybody nana nice noloserick nopuedeser notengoidea nya ooh pablo pancito pencaql picasso picasso2 piropo probando problema que quepaso rage1 rage2 rage3 rata ratero risas roblox runbitch salee salee2 scream simp suck sugoi suspension talisto tarde tepillamos tin tom tontown tranquilo tuturu urss uuh uuy vetealamierda viejaculia watashi waton wena weonculiao willy windows winning xq yabai yabasta yamete yawey yoo"
```

You can write in the chat, the sound name to be played.

# Country flags

Keep in mind that country flags are saved inside zzz-bai-gfx-v8.pk3 file, each flag has its own number (0-249). Uses a bot to identify automatically player's IP and execute the following command:

`setflag #player_id numberofflag`

*Note* : this command can be tested on a server without this bot.

<img src="uploads/c71d78429cae68236204f572a61ce1fc/scoreboardCountryflag.jpg" alt="scoreboardCountryflag" width="500"/>

# References

https://omaera.org/~z411/xonotic/