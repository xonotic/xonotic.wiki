Mapping - Map Picture
=====================

Make your map sexy !  
Your map should include a preview picture. This picture will be visible from the game menu and from the servers vote screen.

In order to do so:
*  Load your map in the game and stay observer
*  Use the command `r_letterbox -1` in the console to turn off the HUD (not `r_letterbox 1` as it'd add top and bottom borders)
*  If your map has "waypoint" entities (like CTF or CTS maps), you can hide them with `cl_hidewaypoints 1`
* You may also want to hide powerups with `g_powerups 0`, as well as weapons, health and armor items with `g_pickup_items 0`. You'll need to reload the map with `restart` for these settings to take effect.
*  Turn off jpeg compression with `scr_screenshot_jpeg 0` to avoid a double lossy compression (you'll have to compress it to jpg later)
*  Take a nice screenshot with F12
*  Go to your Xonotic user directory, on the `data/screenshot` subdirectory. Your user directory location depends on your operating system, more information [here](https://xonotic.org/faq/#config).
*  Open the generated .tga screenshot with an image editor, e.g. GIMP
*  Crop it so that it has a 4:3 ratio, e.g. if resolution is 1366x768 then crop it to 1024x768
*  Scale it to 512x512 px (the resulting image should appear vertically stretched)
*  Save it (export in Gimp) as a JPG file (~90% quality); other formats like tga and png aren't acceptable because they generally produce much bigger files
*  Put it in the same location as the bsp file, with the same name (see next chapter for packaging rules).

Original source of information: https://forums.xonotic.org/showthread.php?tid=6067&pid=75757#pid75757

\<\< [First map](mapping-FirstMap) | [Packaging and releasing](mapping-packaging) \>\>

… [Creating_Maps](Creating-Maps) …