This document applies to communication channels moderated by the Xonotic Team: 
- #xonotic on Freenode's IRC network
- #xonotic on QuakeNet's IRC network
- #xonotic.pickup on QuakeNet's IRC network
- #xonotic.cup on QuakeNet's IRC network
- #xonotic.editing on QuakeNet's IRC network
- All forums found at forums.xonotic.org

The following are prohibited and punishable by banishment:
- Invective targeting persons;
- Threats, incitement or support of abuse or violence;
- Defamation, doxxing, harassment;
- Malicious code or shell commands;
- Spam;
- Links to content prohibited herein;
- Ban evasion.
