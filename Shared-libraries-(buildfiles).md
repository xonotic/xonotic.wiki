darkplaces/Xonotic on Linux uses system libraries. For Windows and macOS external dlls and dylibs are needed. Those reside in xonotic/misc/buildfiles

Because at the time of writing there is no automatism to build or update the games dependencies for those 2 platforms and the current dlls/dylibs are VERY outdated and thus also contain vulnerabilities, this page should document on where to obtain or how to build them, retaining API compatibility to darkplaces and also older OS platform versions like Windows XP.

For Windows, all libraries can/should be obtained from https://packages.msys2.org/ [1] with the exception of an official DLL release from the upstream being available (this is the case for jpeg-turbo and freetype) or if it has to be compiled on under Windows for some reason (eg. libcurl for Schannel (Windows crypto for HTTPS) support).

A handy tool to check dll deps on Windows is https://github.com/lucasg/Dependencies

TODO: macOS

# libcurl
libcurl is used for downloading *.pk3 files from servers

### Windows

```
git clone -b curl-7_80_0 https://github.com/curl/curl || true
curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake
d0=$(pwd)
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DCMAKE_INSTALL_PREFIX="$d0/out" -DCURL_USE_SCHANNEL=YES -G"Unix Makefiles" "$d0/curl"
make
make install
cd "$d0"
cp out/bin/libcurl.dll ~/Games/xonotic/misc/buildfiles/win64/libcurl-4.dll
```

### macOS
Darkplaces loads `libcurl.4.dylib` or `libcurl.3.dylib` or `libcurl.2.dylib`

# libjpeg-turbo
libjpeg-turbo is needed to display jpeg images/textures

### Windows

```
git clone https://github.com/libjpeg-turbo/libjpeg-turbo.git || true
curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake
d0=$(pwd)
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/libjpeg-turbo"
make
make install
cd "$d0"
cp out/bin/libjpeg-62.dll ~/Games/xonotic/misc/buildfiles/win64/libjpeg.dll
```

### macOS
Darkplaces loads `libjpeg.62.dylib`

# zlib
zlib is required to read *.pk3 files. Also it is a dependency of libpng and probably some other libraries.

### Windows
Darkplaces loads: ifdef ZLIB_USES_WINAPI `zlibwapi.dll` or `zlib.dll` else `zlib1.dll`. We use `zlib1.dll`!

Obtainment instructions:
* download as MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-zlib
* use dll from `bin` folder

Cross-compiling:

```
ZLIBVER="1.2.11"
curl -L -o zlib-$ZLIBVER.tar.xz https://zlib.net/zlib-$ZLIBVER.tar.xz || true
tar -xf zlib-$ZLIBVER.tar.xz
rm zlib-$ZLIBVER.tar.xz
curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake

d0=$(pwd)
rm -rf build
mkdir build
cd build
sed -i '/zlib PROPERTIES SUFFIX/i     set_target_properties(zlib PROPERTIES PREFIX "")' "$d0/zlib-$ZLIBVER/CMakeLists.txt" # Remove the "lib" prefix. TODO Do this through the cmake cache?
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DBUILD_SHARED_LIBS=true -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/zlib-$ZLIBVER"
make
make install
cd "$d0"
cp out/bin/zlib1.dll ~/Games/xonotic/misc/buildfiles/win64/zlib1.dll
```

### macOS
Darkplaces loads `libz.dylib`

# libpng
libpng is needed to display png images/textures

### Windows
Darkplaces loads `libpng16.dll` or `libpng16-16.dll` or `libpng15-15.dll` or `libpng15.dll` or `libpng14-14.dll` or `libpng14.dll` or `libpng12.dll`

Obtainment instructions:
* be sure to use a 1.16.X release
* download MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-libpng
* use dll from `bin` folder

Cross-compiling (requires zlib):

```
d0=$(pwd)
PNGVER="libpng16"
ZLIB_ROOT=$d0/../zlib/out # Location to the zlib compiled library

git clone -b $PNGVER git://git.code.sf.net/p/libpng/code libpng || true
curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake

rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DPNG_STATIC=false -DPNG_TESTS=false -DPNG_EXECUTABLES=false -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DZLIB_INCLUDE_DIR=$ZLIB_ROOT/include -DZLIB_LIBRARY=$ZLIB_ROOT/lib/libzlib.dll.a -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/libpng"
make
make install
cd "$d0"
cp out/bin/libpng16.dll ~/Games/xonotic/misc/buildfiles/win64/libpng16.dll
```

### macOS
Darkplaces loads `libpng16.16.dylib` or `libpng15.15.dylib` or `libpng14.14.dylib` or `libpng12.0.dylib`

# libfreetype
Required for the Xolonium font

### Windows
Darkplaces loads: `libfreetype-6.dll` or `freetype6.dll`

```
FTVER="2.11.1"
curl -L -o freetype-$FTVER.tar.xz https://download.savannah.gnu.org/releases/freetype/freetype-$FTVER.tar.xz || true
tar -xf freetype-$FTVER.tar.xz
rm freetype-$FTVER.tar.xz

curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake
d0=$(pwd)
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DBUILD_SHARED_LIBS=true -DCMAKE_BUILD_TYPE=Release -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/freetype-$FTVER"
make
make install
cd "$d0"
cp out/bin/libfreetype.dll ~/Games/xonotic/misc/buildfiles/win64/libfreetype-6.dll
```

### macOS
Darkplaces loads: `libfreetype.6.dylib` or `libfreetype.dylib`

# libogg
libogg is used for the container in cl_capturevideo

### Windows
Darkplaces loads `libogg-0.dll` or `libogg.dll` or `ogg.dll`

Obtainment instructions:
* download MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-libogg
* use dll from `bin` folder

Cross-compiling:

```
d0=$(pwd)
OGGVER="v1.3.5"
git clone -b $OGGVER https://gitlab.xiph.org/xiph/ogg.git
sed -i 's/^LIBRARY ogg$/LIBRARY libogg/' "$d0/ogg/win32/ogg.def" # Call it libogg
curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DBUILD_SHARED_LIBS=true -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/ogg"
make
make install
cd "$d0"
cp out/bin/libogg.dll ~/Games/xonotic/misc/buildfiles/win64/libogg.dll
```

### macOS
Darkplaces loads `libogg.dylib`

# libvorbis + libvorbisfile + libvorbisenc
libvorbis + libvorbisfile are used to play .ogg audio files while libvorbis + libvorbisenc is used for the audio in video capturing (cl_capturevideo)

### Windows
Darkplaces loads `libvorbis-0.dll` or `libvorbis.dll` or `vorbis.dll` and `libvorbisfile-3.dll` or `libvorbisfile.dll` or `vorbisfile.dll` and `libvorbisenc-2.dll` or `vorbisenc-2.dll` or `vorbisenc.dll`

Obtainment instructions:
* download MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-libvorbis
* use dll from `bin` folder

Cross-compiling (requires libogg):

```
d0=$(pwd)
VORBISVER="v1.3.7"
OGG_ROOT=$d0/../ogg/out

git clone -b $VORBISVER https://gitlab.xiph.org/xiph/vorbis.git

# Fix win32 def files
sed -i 's/^LIBRARY$/LIBRARY libvorbis/' "$d0/vorbis/win32/vorbis.def"
sed -i 's/^LIBRARY$/LIBRARY libvorbisenc/' "$d0/vorbis/win32/vorbisenc.def"
sed -i 's/^LIBRARY$/LIBRARY libvorbisfile/' "$d0/vorbis/win32/vorbisfile.def"

curl -o x86_64-w64-mingw32.cmake https://raw.githubusercontent.com/zyga/cmake-toolchains/master/Toolchain-Ubuntu-mingw64.cmake
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="$d0/x86_64-w64-mingw32.cmake" -DBUILD_SHARED_LIBS=true -DOGG_INCLUDE_DIR=$OGG_ROOT/include -DOGG_LIBRARY=$OGG_ROOT/lib/libogg.dll.a -DCMAKE_SYSTEM_PROCESSOR=x86_64 -DCMAKE_INSTALL_PREFIX="$d0/out" -G"Unix Makefiles" "$d0/vorbis"
make
make install
cd "$d0"
cp out/bin/libvorbis.dll ~/Games/xonotic/misc/buildfiles/win64/libvorbis.dll
cp out/bin/libvorbisfile.dll ~/Games/xonotic/misc/buildfiles/win64/libvorbisfile.dll
cp out/bin/libvorbisenc.dll ~/Games/xonotic/misc/buildfiles/win64/libvorbisenc.dll
```

### macOS
Darkplaces loads `libvorbis.dylib` and `libvorbisfile.dylib`

# libtheora
libtheora is used for the video in cl_capturevideo
libtheoraenc/libtheoradec are not needed, they are the newer API; darkplaces uses the legacy pre 1.0 API (libtheora).

### Windows
Darkplaces loads `libtheora-0.dll` or `theora-0.dll` or `theora.dll`

Obtainment instructions:
* download MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-libtheora
* use dll from `bin` folder

Cross-compiling (requires libogg and libvorbis):

```
d0=$(pwd)
THEORAVER="v1.1.1"
OGG_ROOT="$d0/../ogg/out"
VORBIS_ROOT="$d0/../vorbis/out"

git clone -b $THEORAVER https://gitlab.xiph.org/xiph/theora.git

# Fix mingw32 defs
sed -i '1iLIBRARY libtheoradec' "$d0/theora/win32/xmingw32/libtheoradec-all.def"
sed -i '1iLIBRARY libtheoraenc' "$d0/theora/win32/xmingw32/libtheoraenc-all.def"
sed -i '/TH_VP31_QUANT_INFO/d' "$d0/theora/win32/xmingw32/libtheoraenc-all.def"
sed -i '/TH_VP31_HUFF_CODES/d' "$d0/theora/win32/xmingw32/libtheoraenc-all.def"

# Start build
mkdir out
mkdir build
cd build
$d0/theora/autogen.sh --host=x86_64-w64-mingw32 --prefix="$d0/out" --with-ogg="$OGG_ROOT" --with-vorbis="$VORBIS_ROOT" --enable-shared --disable-examples --disable-sdltest --disable-vorbistest --disable-oggtest
make
make install
cd "$d0"
cp out/bin/libtheora-0.dll ~/Games/xonotic/misc/buildfiles/win64/libtheora-0.dll
```

### macOS
Darkplaces loads `libtheora.dylib`

# libgmp
A dependency of libd0_blind_id-0

### Linux

`./configure --prefix=$HOME/Games/xonotic/misc/builddeps/linux64/gmp --enable-static --disable-shared --with-pic --enable-fat`

### Windows
libd0_blind_id-0 loads `libgmp-10.dll`

Obtainment instructions:
* download as MSYS2 Package [1] in x86 and x64 versions: https://packages.msys2.org/base/mingw-w64-gmp
* use dll from `bin` folder

Cross-compiling:

```
d0=$(pwd)
GMPVER="6.2.1"

curl -o gmp-$GMPVER.tar.xz https://gmplib.org/download/gmp/gmp-$GMPVER.tar.xz
tar xf gmp-$GMPVER.tar.xz
rm gmp-$GMPVER.tar.xz

mkdir out
mkdir build
cd build
"$d0/gmp-$GMPVER/configure" --prefix="$d0/out" --host=x86_64-w64-mingw32 --with-pic --enable-fat --disable-static --enable-shared
make
make install
cd "$d0"
cp out/bin/libgmp-10.dll ~/Games/xonotic/misc/buildfiles/win64/libgmp-10.dll
```

# libd0_blind_id-0 & libd0_rijndael-0
Internal project, see https://gitlab.com/xonotic/d0_blind_id

### Linux

`LDFLAGS='-L$HOME/Games/xonotic/misc/builddeps/linux64/gmp/lib' CPPFLAGS='-I$HOME/Games/xonotic/misc/builddeps/linux64/gmp/include' ./configure --prefix=$HOME/Games/xonotic/misc/builddeps/linux64/d0_blind_id  --enable-static --disable-shared --with-pic`

### Windows
Cross-compiling (requires libgmp):

```
d0=$(pwd)
GMP_ROOT="$d0/../gmp/out"
export LDFLAGS="-L$GMP_ROOT/lib"
export CPPFLAGS="-I$GMP_ROOT/include"

git clone https://gitlab.com/xonotic/d0_blind_id.git

cd d0_blind_id
./autogen.sh
sed -i '/libd0_blind_id_la_LDFLAGS/ s/$/ -no-undefined/' Makefile.am
sed -i '/libd0_rijndael_la_LDFLAGS/ s/$/ -no-undefined/' Makefile.am

cd "$d0"
mkdir build
mkdir out
cd build
"$d0/d0_blind_id/configure" --with-pic --prefix="$d0/out" --host=x86_64-w64-mingw32
make
make install

cd "$d0"
cp out/bin/libd0_blind_id-0.dll ~/Games/xonotic/misc/buildfiles/win64/libd0_blind_id-0.dll
cp out/bin/libd0_rijndael-0.dll ~/Games/xonotic/misc/buildfiles/win64/libd0_rijndael-0.dll
```

# libode
Is not loaded under Windows and crashes the game if it is and a map is loaded up.
Also it is not statically linked and thus requires libstdc++-6.dll and libgcc_s_sjlj-1.dll.

### Linux:

`./configure --enable-static --disable-shared --with-libccd=internal --enable-double-precision --prefix=$HOME/Games/xonotic/misc/builddeps/linux64/ode --with-pic`

# libavw
*Note:* Old and not used in Xonotic but also not disabled :) Adding this for the sake of completeness.

### Windows
Darkplaces loads: `libavw.dll`

Source code: https://github.com/paulvortex/DpLibAVW

### macOS
Darkplaces loads: `libavw.dylib`