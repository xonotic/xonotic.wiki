Items
=====

In the arena you will find various items lying around, waiting to be collected.

Health
------

Health items increase your health by a certain amount. With health items you can reach a maximum health of 200.

| **Name** | Mega Health | Big Health | Medium Health | Small Health |
|:-|:-:|:-:|:-:|:-:|
| **Health** | 100 | 50 | 25 | 5 |
| **Item image** |![][h_mega] | ![][h_big] |![][h_medium] | ![][h_small] |

[h_mega]: uploads/c3466c97c10a16b06b8c523b22a0e0ab/health_mega.png
[h_big]: uploads/b7538e50554b70d4c8c0946e4c6cd991/health_big.png
[h_medium]: uploads/968d5da002ae4e9070381e09c833504e/health_medium.png
[h_small]: uploads/37a186e2bba7428e12d3f43a9f54971a/health_small.png

Armor
-----

Armor items increase your armor by a certain amount. With armor items you can reach a maximum armor of 200.

| **Name** | Mega Armor | Big Armor | Medium Armor | Small Armor |
|:-|:-:|:-:|:-:|:-:|
| **Armor** | 100 | 50 | 25 | 5 |
| **Item image** |![][a_mega] | ![][a_big] |![][a_medium] | ![][a_small] |

[a_mega]: uploads/ba42b9e7a7619c82f97eca0be5b0efc6/armor_mega.png
[a_big]: uploads/e7c23520812595b949077456d0e0b002/armor_big.png
[a_medium]: uploads/46eaf8713d4ad22caec4028948a815a0/armor_medium.png
[a_small]: uploads/4fd1b9f684461c71bc412f9db18cddc2/armor_small.png


Ammo
----

Ammo is required by most [weapons](Weapons).

| **Name** | Shells | Bullets | Cells | Rockets |
|:-|:-:|:-:|:-:|:-:|
| **Amount** | 15 | 80 | 30 | 40 |
| **Max. amount** | 60 | 320 | 180 | 160 |
| **Item image** |![][shells]  |  ![][bullets]  |![][cells] |  ![][rockets]|
| **Icon** |![][shells_luma]  |  ![][bullets_luma]  |![][cells_luma] |  ![][rockets_luma]|

[shells]: http://pics.nexuizninjaz.com/images/q32119stqpps94ohxs.png
[bullets]: http://pics.nexuizninjaz.com/images/ryedjz1ljsdi0upidhz.png
[cells]: http://pics.nexuizninjaz.com/images/xj7eitmvqar0eya0b9h.png
[rockets]: http://pics.nexuizninjaz.com/images/rltbq9d2v2ao425q20he.png

[shells_luma]: http://pics.nexuizninjaz.com/images/8zuasyijthfo9qnjh236.png
[bullets_luma]: http://pics.nexuizninjaz.com/images/if6j7j62k3ycagfdss4n.png
[cells_luma]: http://pics.nexuizninjaz.com/images/nbk02sbuc9jfo6570pbk.png
[rockets_luma]: http://pics.nexuizninjaz.com/images/wci2civtjylb4pejoe9b.png

Fuel
----

Fuel is used for special tools like the [Jetpack](jetpack). In the HUD, fuel is displayed as a yellow bar.

The fuel item will refill your fuel completely.

Depending on game settings, fuel may also automatically refill when you're low on fuel, at least partially.

| **Name** | Fuel |
|:-|:-:|
| **Item image** | ![][fuel] |
| **Icon** | ![][fuel_luma] |

[fuel]: http://pics.nexuizninjaz.com/images/05moogdlts8nce1f677.png
[fuel_luma]: http://pics.nexuizninjaz.com/images/r48z28t4z42sp003ch2r.png

See also
--------
* [Powerups](Powerups)