## How can I block/ignore people personally?

**As a player**, use `ignore #player_id`, this player will be blocked and entirely hidden from your personal chat logs. \
Keep in mind that this can result in confusing situations as the blocked player(s) can continue talking to others in the server while you've blocked them. \
To unblock a single player, use `unignore #player_id`. \
To unblock all currently ignored players, use `clear_ignores`.

_Note: you can't ignore more than 16 players._

## How can I disallow people from chatting in my server?

**As a server admin**, if there's someone spamming in the chat all time or being hostile, use `chatban #player_id` to prevent them from being able to send any messages to anyone, this includes private messages, team chat and spectator chat. \
To unban the chatbanned player, use `unchatban #player_id`. \
To see the list of chatbanned players, use `chatbans`.

_Note 1: there's another aliased version: `mute`. It has been used for many years while it wasn't improved, only temporarily muting for the duration of the match._ \
_Note 2: `muteban` is an alias to `chatban` as it was this feature's previous name and has been used for many years by a few admins while it was implemented via_ [_SMB modpack_](https://github.com/MarioSMB/modpack) _before its merge to Xonotic's official repositories._

Set `g_chat_allowed` to 0, if you think there is a good reason to not anyone use any chats. \
Remember, you can disable private, spectator and team chat as you wish.

- `g_chat_private_allowed 0` disables private chat, players can't use `tell #player_id` to send a private message to someone.
- `g_chat_spectator_allowed 0` players can't chat while spectating.
- `g_chat_team_allowed 0` disables team chat, preventing players from talking privately in their own teams.

These cvars are set to 1 by default. Set any of them to 0 if you wish to disallow that chat. 

## How can I disallow people from voting in my server?

**As a server admin**, use `voteban #player_id` to disallow a player from voting, they can't start a vote nor vote Yes or No for votes called by other players. \
To allow the votebanned player vote again, use `unvoteban #player_id`.\
To see the list of disallowed players from voting, use `votebans`. 

## How can I disallow people from playing in my server?

**As a server admin**, use `playban #player_id` to disallow a player from playing in the server, this player will be forced to spectate. \
To allow a player to play again instead of being forced to spectate, use `unplayban #player_id`.\
To see the list of players forced to spectate, use `playbans`. 

_Note: `g_playban_minigames 1` also prevents the playbanned players from participating in minigames. (This is disabled by default)._

## How can I ban people from my server?

**As a server admin**, doing that can be pretty harsh. Use this last resource if you decided definitely that it is what you want to do. \
`ban <player_IP_address/key_hash> <bantime in seconds> <reason>` stores a ban in the server, it isn't a quick ban. The player can stay in the server unless if disconnects and tries to go back. \
`kickban #player_id` stores a ban and kicks the player from the server. \
To unban the player, use `unban <player_IP_address/key_hash>`. \
To see the ban list, use `bans`.

## TL;DR:

#### Personally as a player

- block in chat
  * `ignore #player_id`
- unblock in chat
  * `unignore #player_id`
- List blocks
  * Can't in-game.

#### In a server as an admin or a moderator

- Ban
  * `chatban #player_id`
  * `voteban #player_id`
  * `playban #player_id`
- Unban
  * `unchatban #player_id`
  * `unvoteban #player_id`
  * `unplayban #player_id`
- List bans
  * `chatbans`
  * `votebans`
  * `playbans`

#### From the whole server

- Kick and/or ban from the server
  * `kick #player_id`
  * `ban <player_IP_address/key_hash> <bantime in seconds> <reason>`
  * `kickban #player_id`
- Unban from the server
  * `unban <player_IP_address/key_hash>`
- List bans from the server
  * `bans`