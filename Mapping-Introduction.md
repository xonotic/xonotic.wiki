Mapping - Introduction
======================

Welcome to the wonderful world of mapping, this tutorial will lead you through the fundamentals of mapping for Xonotic.
Many of these tutorials may also useful for other games that use the quake3map2 map format.

REQUIREMENTS
------------

-   A computer that can run Xonotic.
-   If you use an old version of NetRadiant or another map editor (including NetRadiant-custom, which is a fork of NetRadiant), you need the [mapping support package](http://dl.xonotic.org/xonotic-0.8.5-mappingsupport.zip). It isn't needed with the latest NetRadiant builds.
-   Time.

[Setup](mapping-Setup) \>\>

… [Creating_Maps](Creating-Maps) …