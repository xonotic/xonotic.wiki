## Overview

While this page will focus on the official monsters that ship with the game, other monsters from popular Xonotic mods will be included. Some monsters described in this section come from Quake and the descriptions for those are mirrored from this [wiki](https://quakewiki.org/wiki/Category:Monsters).

_Note: Some features may be missing from this wiki page_.

# Vanilla monsters

## Zombie

![xonotic20230616230630-00.jpg](uploads/0c1313aad39b94f00db093f54d379c1c/xonotic20230616230630-00.jpg)

Zombie is an undead human that tears chunks of flesh from its head, hindquarters, and crotch before proceeding to toss them at the player character for minor damage. When someone comes close to the zombie, the zombie will start chasing and launching to attack with a bite. When the zombie is in a dead pose in battle, can be revived and continue as before. To kill him, you will need to shoot / hit the zombie when you see it in a dead pose during the battle.

## Spider

![xonotic20230616230916-00.jpg](uploads/ec76013f8f6c39e5d3d9ad182818454c/xonotic20230616230916-00.jpg)

Spider is a big arachnid that walks with 8 legs on the ground, can walk faster than other monsters. When someone approaches the spider, the spider will spit cobweb energy balls to slow down prey and attack nearby with its chelicerae.

## Wyvern

![xonotic20230616230906-00.jpg](uploads/60ed6217d9608f9260e2306a039a5402/xonotic20230616230906-00.jpg)

Wyvern is a dragon that was said to breathe fire or to possess a poisonous breath, a reptilian body, two legs or sometimes none, and a spiked tail. The alleged marine variant had a fishtail instead of the spiny dragon tail. Wyvern flies around the map, when someone approaches this monster, the opponent will be chased and attacked by his fireballs.

## Shambler

![xonotic20230616230550-00.jpg](uploads/3472a53956c46725e1190936d916abcd/xonotic20230616230550-00.jpg)

Shamblers are truly a force to be feared. They tend to have an immense amount of health and take only half the damage from explosions. His pride and joy is his lightning energy attack. His attacks are throwing electro balls, if the prey is near to him, he will attack with his claws.

_Note: The internal file is named "golem.qc"_

## Mage

![4eg.jpg](uploads/801998e5644db63a5946c46a29ddec85/4eg.jpg)

Mage is capable of developing magic with his powerful knowledge that he learned and practiced during his spell sciences. Sometimes the mage can give regeneration to the owner and sometimes makes the owner pay the health for his needs to be with him. The mage attacks launching magic balls, this spell attack is called as "Mage spike".

# Unofficial monsters

## Afrit

![afrit](uploads/b77bc014b93ea6f97e581be4eece805d/afrit.jpg){width="290"}

Afrit is a dragon / flying demon that covers the flames around its body. His fire power is strong but his movements are a little slow. Afrit can see the prey as it gets closer, when it comes, Afrit will start to follow and attack with his fireballs to the target.

## Creeper

![creeper](uploads/48f09eb2f3608e502ad88c348a4b4473/creeper.jpg){width="280"}

The same things as the mage, but only attacks nearby to the prey, looks like hitting. This info is still unknown.

## Demon

![demon](uploads/56b672332778d67b7c359b611d54c06f/demon.jpg){width="290"}

Demon is a four-legged, monkey-like beast that uses its claws to slash a target. In addition, it is very fast and can leap great distances. Demons stand approximately 6 feet tall (their back rises above their head) and have a muscular appearance. With his mouth full of sharp teeth, he can gut a living being.

## Enforcer

![enforcer](uploads/00fe4f08ebbb70f7ec3a0c9d4324d61b/enforcer.jpg){width="270"}

Enforcer is an upgraded Grunt wearing a Biosuit and is equipped with a Laser Rifle. Enforcers will fire two lasers as soon as they see you. Each laser will disappear within six seconds if it does not make contact with another entity. The lasers can be easily avoided. Due to their slow nature, it's easy to avoid the lasers and manage to attack without any difficulty.

## Ogre

![ogre](uploads/57bf95ee9075a32117999d7348cd4e8d/ogre.jpg){width="270"}

Ogres carry a deadly chainsaw for melee fights and a grenade launcher otherwise (though their grenade launcher is weaker than the player's), they can truly pack a punch.

## Scrag

![scrag](uploads/967d1e4b8c364b86fad171a959f5f5f9/scrag.jpg){width="290"}

Scrags possess some cunning unlike other monsters. They are constantly moving side to side (except when attacking), and so manage to dodge many incoming slow-moving projectiles. Also, if you duck behind cover while they are attacking, they may throw an extra shot or two at your last seen location, so you might want to be careful before you pop back out again.

## Soldier

![soldier](uploads/167212cbdeca5b838efdcbc0757259d8/soldier.jpg){width="260"}

Soldier is an abandoned military warrior with grudges and a desire to kill others. His weapons aren't strong, but he can aim well at the target and control their base where he is prowling.

# Notes for players

To spawn a monster, either:

* Press F8 \>\> Select Spawn monster \>\> And monster is spawned in some place of the map
* Create a map with a monster spawn point in it

# Notes for developers

**Official monster source codes are in this **[**directory**](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/tree/master/qcsrc/common/monsters/monster)**.** Unofficial monster source code is stored in the [SMB modpack](https://github.com/MarioSMB/modpack/tree/master/mod/common/monsters).